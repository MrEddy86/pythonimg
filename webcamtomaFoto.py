import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
    # captura frame 
    ret, frame = cap.read()

    # convierte frame a grises
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # muestra la imagen
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        print('vamo a salir')
        break
    if cv2.waitKey(1) == ord('a'):
        cv2.imwrite('imggray.png',frame)
        print('imagen capturada')
        

    
cap.release()
cv2.destroyAllWindows()
